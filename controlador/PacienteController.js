'use strict';
var pacienteC = require('../modelos/paciente');
var historiaC = require('../modelos/historia');
class PacienteController {
    visualizar(req, res) {
        pacienteC.getJoin({historia: true}).then(function (todos) {            
            var nro = "NHIS-" + (todos.length + 1);            
            res.render('index',
                    {title: 'Pacientes',
                        andrea: "fragmentos/paciente/lista",
                        sesion: true,
                        listado: todos,
                        nro: nro,
                        msg: {error: req.flash('error'), info: req.flash('info')}
                    });
        }).error(function (error) {
            req.flash('error', 'Hubo un error!');
            res.redirect('/');
        });
    }
    
    guardar(req, res) {
        var dataP = {
            cedula: req.body.cedula,
            apellidos: req.body.apellidos,
            nombres: req.body.nombres,
            fecha_nac: req.body.fecha_nac,
            edad: req.body.edad,
            direccion: req.body.direccion
        };
        var paciente = new pacienteC(dataP);
        var dataH = {
            nro_historia: req.body.nro_his,
            enfermedades: req.body.enf,
            enfer_hede: req.body.enf_her,
            habitos: req.body.hab,
            contacto: req.body.contacto
        };
        var historia = new historiaC(dataH);
        paciente.historia = historia;
        paciente.saveAll({historia: true}).then(function (nuevo) {
            req.flash('info', 'Paciente registrado!');
            res.redirect('/administracion/pacientes');
        }).error(function (error) {
            req.flash('error', 'No se pudo registrar!');
            res.redirect('/administracion/pacientes');
        });
    }

}
module.exports = PacienteController;