'use strict';
var medico = require('../modelos/medico');
var cuenta = require('../modelos/cuenta');
class Medicocontroller {
    verRegistro(req, res) {
        res.render('index', {title: 'Registrate', sesion: true, andrea: "fragmentos/registro_medico"});
    }

    guardar(req, res) {
        var datos = {
            cedula: req.body.cedula,
            apellidos: req.body.apellidos,
            nombres: req.body.nombres,
            fecha_nac: req.body.fecha_nac,
            edad: req.body.edad,
            nro_registro: req.body.nro,
            especialdidad: req.body.especialidad
        };

        var datosC = {
            correo: req.body.correo,
            clave: req.body.clave
        };

        var Medico = new medico(datos);
        var Cuenta = new cuenta(datosC);
        Medico.cuenta = Cuenta;
        Medico.saveAll({cuenta: true}).then(function (result) {
            //res.send(result);
            req.flash('success', 'Se ha registrado correctamente!');
            res.redirect('/');
        }).error(function (error) {
            res.send(error);
        });

    }
}
module.exports = Medicocontroller;







